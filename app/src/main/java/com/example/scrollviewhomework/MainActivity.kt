package com.example.scrollviewhomework

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import com.example.scrollviewhomework.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        for(i in 1..50){
            val textView = TextView(this)
            textView.text = "Hello World $i"
            textView.textSize = 14f
            textView.setTextColor(Color.rgb(128, 64 + i * 2, 128))
            textView.gravity = Gravity.CENTER_HORIZONTAL

            binding.listViewInScrollView.addView(textView)
        }
    }
}
